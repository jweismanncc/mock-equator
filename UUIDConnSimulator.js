/*
In the node.js intro tutorial (http://nodejs.org/), they show a basic tcp 
server, but for some reason omit a client connecting to it.  I added an 
example at the bottom.

Save the following server in example.js:
 */
/* 
 var net = require('net');

 var server = net.createServer(function(socket) {
 socket.write('Echo server\r\n');
 socket.pipe(socket);
 });

 server.listen(1337, '127.0.0.1');
 */
/*
 And connect with a tcp client from the command line using netcat, the *nix 
 utility for reading and writing across tcp/udp network connections.  I've only 
 used it for debugging myself.

 $ netcat 127.0.0.1 1337

 You should see:
 > Echo server

 */

/* Or use this example tcp client written in node.js.  (Originated with 
 example code from 
 http://www.hacksparrow.com/tcp-socket-programming-in-node-js.html.) */

var net = require('net');
var fs = require("fs");
// var Buffer = require('buffer');
// var Cutter = require('utf8-binary-cutter');

var HOST = 'fusionaws.clarecontrols.com';
var PORT = 8888;
var EQUATORS_NO = 170;
var deploying = [];
var previousRequestIds = [];
var wstreams = [];
var wstreamrequests = [];

var UUIDSFile = require('./equatorUUID.js');
var EQUATORS = []; 

//var client = new net.Socket();
var clients = [];
var i = 0;
UUIDSFile.UUIDs.forEach(function(value) {
	if (i < EQUATORS_NO){
	 EQUATORS.push(value);
	}
	i++;
});

EQUATORS.forEach(function(value){
	var client = new net.Socket();
	clients.push(client);
	previousRequestIds.push(0);
	deploying.push(0);

	var wstream = fs.createWriteStream("./docs/"+value);
	var wstreamrequest = fs.createWriteStream("./docs/"+value+"_request");
	wstreams.push(wstream);
	wstreamrequests.push(wstreamrequest);
	
	
	var index = clients.indexOf(client);
	
	client.connect(PORT, HOST, function() {
		console.log('Connecting ...-'+value);
		// send handshake
		
		fs.readFile("./mock_data/handshake_UUID.json", function(err, reply) {
			//console.log('Sending ...');
			
			var replystr = reply.toString();
			replystr = replystr.replace(
					'%UUID%',
					value);
			//console.log("Reply is "
			//		+ replystr);

			var messageBuf = new Buffer(
					replystr, 'utf-8');
			//console.log(messageBuf.length);
			
			var sizeBuf = new Buffer(4);
			sizeBuf.writeUInt32BE(messageBuf.length, 0);

			client.write(sizeBuf);
			//console.log(sizeBuf);
			client.write(messageBuf);
			//console.log(messageBuf);
		});
	});
	client.on('data',function(data) {

		// switch on the various different messages
		console.log('--> Received RAW: ' + data);

		// First 4 bytes are the message size
		var sizeBuf = new Buffer(4);
		sizeBuf[0] = data[0];
		sizeBuf[1] = data[1];
		sizeBuf[2] = data[2];
		sizeBuf[3] = data[3];

		var size = sizeBuf.readUInt32BE(0);
		//console.log("Next message size:" + size);

		// The rest of the message is the actual message
		if (size <= 1024) {
			
			var messageBuf;
		
			messageBuf = new Buffer(size);
			data.copy(messageBuf, 0, 4);

			// Parse it as a json object
			var jsonobj;
			jsonobj = JSON.parse(messageBuf);
		
			if (jsonobj.result != null && jsonobj.result == "OK") {
				//console.log("Received OK!");
				console.log('Connected-'+value);

			} else if (jsonobj.method != null
					&& jsonobj.method == "HDAdmin/getProtocolAdapterInfos") {
				// reply
				var requestid = jsonobj.id;
				console
						.log("Received getProtocolAdapterInfos request with id:"
								+ requestid);

				fs.readFile("./mock_data/HDAdmin_getProtocolAdapterInfos_UUID.json",
						function(err, reply) {
							var replystr = reply.toString();
							replystr = replystr.replace(
									'%REQUESTID%',
									requestid);
							replystr = replystr.replace(
									'%UUID%',
									value);
							//console.log("Reply is "
							//		+ replystr);
							var messageBuf = new Buffer(
									replystr, 'utf-8');
							//console.log(messageBuf.length);

							var sizeBuf = new Buffer(4);
							sizeBuf.writeUInt32BE(
									messageBuf.length, 0);

							client.write(sizeBuf);
							// console.log(sizeBuf);
							client.write(messageBuf);
							// console.log(messageBuf);
						});

			} else if (jsonobj.method != null
					&& jsonobj.method == "HAM/getCommandTypes") {
				// reply
				var requestid = jsonobj.id;
				console
						.log("Received getCommandTypes request with id:"
								+ requestid);

				fs.readFile("./mock_data/HAM_getCommandTypes_UUID.json",
						function(err, reply) {

					var replystr = reply.toString();
					replystr = replystr.replace(
							'%REQUESTID%',
							requestid);
					replystr = replystr.replace(
							'%UUID%',
							value);
					//console.log("Reply is "
					//		+ replystr);

					var messageBuf = new Buffer(
							replystr, 'utf-8');
					//console.log(messageBuf.length);

					var sizeBuf = new Buffer(4);
					sizeBuf.writeUInt32BE(
							messageBuf.length, 0);

					client.write(sizeBuf);
					//console.log(sizeBuf);
					client.write(messageBuf);
					//console.log(messageBuf);
				});
			} else if (jsonobj.method != null
					&& jsonobj.method == "HAM/getConditionTypes") {
				var requestid = jsonobj.id;
				console.log("Received getConditionTypes request with id:"
								+ requestid);

				fs.readFile("./mock_data/HAM_getConditionTypes_UUID.json",
						function(err, reply) {
							var replystr = reply.toString();
							replystr = replystr.replace(
									'%REQUESTID%',
									requestid);
							replystr = replystr.replace(
									'%UUID%',
									value);
							//console.log("Reply is "
							//		+ replystr);

							var messageBuf = new Buffer(
									replystr, 'utf-8');
							//console.log(messageBuf.length);
		
							var sizeBuf = new Buffer(4);
							sizeBuf.writeUInt32BE(
									messageBuf.length, 0);
		
							client.write(sizeBuf);
							//console.log(sizeBuf);
							client.write(messageBuf);
							//console.log(messageBuf);
						});
			} else if (jsonobj.method != null
			  		&& jsonobj.method == "configuration.import") {
				console.log("Received deploy request (All in One Message < 1024) for "+value);
				
				//Delete files if the already exist
				if(fs.existsSync("./docs/"+value)){
					fs.unlinkSync("./docs/"+value);
					console.log("successfully deleted "+ "./docs/"+value);
				}
				if(fs.existsSync("./docs/"+value+"_request")){
					fs.unlinkSync("./docs/"+value+"_request");
					console.log("successfully deleted "+ "./docs/"+ value +"_request");
				}
				if(fs.existsSync("./docs/"+value+".zip")){
					fs.unlinkSync("./docs/"+value+".zip");
					console.log("successfully deleted "+ "./docs/"+ value +".zip");
				}
				
				//create new write streams
				wstreams[index] = fs.createWriteStream("./docs/"+value);
				wstreamrequests[index] = fs.createWriteStream("./docs/"+value+"_request");
				
				//All in one message
				//Get the requestId
				var startIndex = data.toString().indexOf("{\"id\":");
				var endIndex = data.toString().indexOf(",\"method\":\"configuration.import\",");
				previousRequestIds[index] = data.toString().substring(startIndex+6, endIndex); 
				
				//Saving the complete request
				wstreamrequests[index].write(data);
				
				//Keep only the deployment data part
				var dataIndexStart = data.toString().indexOf(":[\"");
				data = data.toString().substring(dataIndexStart+3); 
				var dataIndexEnd = data.toString().indexOf("\"]");
				data = data.toString().substring(0, dataIndexEnd);
				
				//Save it in the deployment data file
				wstreams[index].write(data);
				
				//Flush it in the file
				wstreamrequests[index].end();
				wstreams[index].end(function(){
					//decode
					var encodedFile = fs.readFileSync("./docs/"+value, 'utf8');
					var decodedBuffer = new Buffer(encodedFile, 'base64');
					fs.writeFileSync("./docs/"+value+".zip", decodedBuffer);
				});
			
				fs.readFile("./mock_data/configurationImportReply_UUID.json",
						function(err, reply) {
							var replystr = reply.toString();
							replystr = replystr.replace(
									'%REQUESTID%',
									previousRequestIds[index]);
							replystr = replystr.replace(
									'%UUID%',
									value);
							//console.log("Reply is "
							//		+ replystr);

							var messageBuf = new Buffer(
									replystr, 'utf-8');
							//console.log(messageBuf.length);

							var sizeBuf = new Buffer(4);
							sizeBuf.writeUInt32BE(
									messageBuf.length, 0);

							client.write(sizeBuf);
							// console.log(sizeBuf);
							client.write(messageBuf);
							// console.log(messageBuf);
							console
							.log("Deployed to: "+value);


						});
				deploying[index] = 0;
			}// else if (deploying[index] ==1 && data.toString().indexOf("\"uuid\":\"" +value + "\",\"jsonrpc\":\"2.0\"}") > -1){
			 else if (deploying[index] ==1 && data.toString().indexOf("jsonrpc\":\"2.0\"}") > -1){
					
				//This is the last message
				
				//Saving the complete request
				wstreamrequests[index].write(data);
				
				//Keep only the deployment data part
				var dataIndexEnd = data.toString().indexOf("\"]");
				if(dataIndexEnd>-1){
					data = data.toString().substring(0, dataIndexEnd);
					wstreams[index].write(data);
				}
				
				wstreamrequests[index].end();
				wstreams[index].end(function(){
					//decode
					var encodedFile = fs.readFileSync("./docs/"+value, 'utf8');
					var decodedBuffer = new Buffer(encodedFile, 'base64');
					fs.writeFileSync("./docs/"+value+".zip", decodedBuffer);
				});

				fs.readFile("./mock_data/configurationImportReply_UUID.json",
							function(err, reply) {
									var replystr = reply.toString();
									replystr = replystr.replace(
											'%REQUESTID%',
											previousRequestIds[index]);
									replystr = replystr.replace(
											'%UUID%',
											value);
									//console.log("Reply is "
									//		+ replystr);

									var messageBuf = new Buffer(
											replystr, 'utf-8');
									//console.log(messageBuf.length);

									var sizeBuf = new Buffer(4);
									sizeBuf.writeUInt32BE(
											messageBuf.length, 0);

									client.write(sizeBuf);
									// console.log(sizeBuf);
									client.write(messageBuf);
									// console.log(messageBuf);
									console
									.log("Deployed to: "+value);


								});
				deploying[index] = 0;
				}
		}// if size < =1024

		else {
			try{
			if (deploying[index] == 0) {
				console.log("Deploying ==0");
				console
				.log("Received deploy request (Initial message)  for "+value);
				//First big message, get the request id
				
				if (data.toString().indexOf("configuration.import") > -1) {
					if(fs.existsSync("./docs/"+value)){
						fs.unlinkSync("./docs/"+value);
						console.log("successfully deleted "+ "./docs/"+value);
					}
					if(fs.existsSync("./docs/"+value+"_request")){
					fs.unlinkSync("./docs/"+value+"_request");
					console.log("successfully deleted "+ "./docs/"+ value +"_request");
					}
					if(fs.existsSync("./docs/"+value+".zip")){
					fs.unlinkSync("./docs/"+value+".zip");
					console.log("successfully deleted "+ "./docs/"+ value +".zip");
					}
					
					wstreams[index] = fs.createWriteStream("./docs/"+value);
					wstreamrequests[index] = fs.createWriteStream("./docs/"+value+"_request");
				
					deploying[index] = 1;
					var startIndex = data.toString().indexOf("{\"id\":");
					var endIndex = data.toString().indexOf(",\"method\":\"configuration.import\",");
					
					previousRequestIds[index] = data.toString().substring(startIndex+6, endIndex); 
					
					//Start saving the file sent, the file is saved as a binary file
					wstreamrequests[index].write(data);
					var dataIndexStart = data.toString().indexOf(":[\"");
					data = data.toString().substring(dataIndexStart+3); 
					
					
					// check if all messages are in one 
					if (data.toString().indexOf("\"uuid\":\"" +value + "\",\"jsonrpc\":\"2.0\"}") > -1){
						console
						.log(".... it is ALL in one message for "+value);
						//Start saving the file sent, the file is saved as a binary file
						
						var dataIndexEnd = data.toString().indexOf("\"]");
						data = data.toString().substring(0, dataIndexEnd);
						wstreams[index].write(data);
						
						wstreamrequests[index].end();
						wstreams[index].end(function(){
							//decode
							var encodedFile = fs.readFileSync("./docs/"+value, 'utf8');
							var decodedBuffer = new Buffer(encodedFile, 'base64');
							fs.writeFileSync("./docs/"+value+".zip", decodedBuffer);
						});
						fs.readFile("./mock_data/configurationImportReply_UUID.json",
									function(err, reply) {
										var replystr = reply.toString();
										replystr = replystr.replace(
												'%REQUESTID%',
												previousRequestIds[index]);
										replystr = replystr.replace(
												'%UUID%',
												value);
										//console.log("Reply is "
										//		+ replystr);

										var messageBuf = new Buffer(
												replystr, 'utf-8');
										//console.log(messageBuf.length);

										var sizeBuf = new Buffer(4);
										sizeBuf.writeUInt32BE(
												messageBuf.length, 0);

										client.write(sizeBuf);
										// console.log(sizeBuf);
										client.write(messageBuf);
										// console.log(messageBuf);
										console
										.log("Deployed to: "+value);


									});
						deploying[index] = 0;
					}
					else{
						var dataIndexEnd = data.toString().indexOf("\"]");
						if(dataIndexEnd>-1){
							data = data.toString().substring(0, dataIndexEnd);
						}
						wstreams[index].write(data);
					}
					
				}
			}
			//deploying ==1
			else{
				console.log("Deploying ==1");
					//if (data.toString().indexOf("\"uuid\":\"" +value + "\",\"jsonrpc\":\"2.0\"}") > -1){
					if (data.toString().indexOf("jsonrpc\":\"2.0\"}") > -1){
						
						console
								.log("Received last deploy message of many for "+value);
						//Start saving the file sent, the file is saved as a binary file
						wstreamrequests[index].write(data);
					
						var dataIndexEnd = data.toString().indexOf("\"]");
						if(dataIndexEnd>-1){
							data = data.toString().substring(0, dataIndexEnd);
							wstreams[index].write(data);
						}
						
						wstreamrequests[index].end();
						wstreams[index].end(function(){
							//decode
							var encodedFile = fs.readFileSync("./docs/"+value, 'utf8');
							var decodedBuffer = new Buffer(encodedFile, 'base64');
							fs.writeFileSync("./docs/"+value+".zip", decodedBuffer);
						});
						fs.readFile("./mock_data/configurationImportReply_UUID.json",
									function(err, reply) {
										var replystr = reply.toString();
										replystr = replystr.replace(
												'%REQUESTID%',
												previousRequestIds[index]);
										replystr = replystr.replace(
												'%UUID%',
												value);
										//console.log("Reply is "
										//		+ replystr);

										var messageBuf = new Buffer(
												replystr, 'utf-8');
										//console.log(messageBuf.length);

										var sizeBuf = new Buffer(4);
										sizeBuf.writeUInt32BE(
												messageBuf.length, 0);

										client.write(sizeBuf);
										// console.log(sizeBuf);
										client.write(messageBuf);
										// console.log(messageBuf);
										console
										.log("Deployed to: "+value);


									});
						deploying[index] = 0;
						}else{
							//deploying, not first or last message
							var dataIndexEnd = data.toString().indexOf("\"]");
							if(dataIndexEnd>-1){
								data = data.toString().substring(0, dataIndexEnd);
							}
							wstreams[index].write(data);
							wstreamrequests[index].write(data);
							console
							.log("Multiple messages sent to: "+value);
							//console
							//.log("Contents are:" + data.toString());
							
						
						}	
					}
				}catch(e)
				{
					console.log(e);
				}
			}

	});

	client.on('error', function(e) {
		if(e.code == 'ECONNREFUSED') {
	        console.log('Is the server running at ' + PORT + '?');

	        client.setTimeout(4000, function() {
	            client.connect(PORT, HOST, function(){
	                console.log('CONNECTED TO: ' + HOST + ':' + PORT);
	                
	                fs.readFile("./mock_data/handshake_UUID.json", function(err, reply) {
	        			//console.log('Sending ...');
	        			
	        			var replystr = reply.toString();
	        			replystr = replystr.replace(
	        					'%UUID%',
	        					value);
	        			//console.log("Reply is "
	        			//		+ replystr);

	        			var messageBuf = new Buffer(
	        					replystr, 'utf-8');
	        			//console.log(messageBuf.length);
	        			
	        			var sizeBuf = new Buffer(4);
	        			sizeBuf.writeUInt32BE(messageBuf.length, 0);

	        			client.write(sizeBuf);
	        			//console.log(sizeBuf);
	        			client.write(messageBuf);
	        			//console.log(messageBuf);
	        		});
	         
	            });
	        });

	        console.log('Timeout for 5 seconds before trying port:' + PORT + ' again');

	    }  else if(e.code == 'ECONNRESET'){
	    	client.setTimeout(4000, function() {
	            client.connect(PORT, HOST, function(){
	                console.log('CONNECTED TO: ' + HOST + ':' + PORT);
	                fs.readFile("./mock_data/handshake_UUID.json", function(err, reply) {
	        			//console.log('Sending ...');
	        			
	        			var replystr = reply.toString();
	        			replystr = replystr.replace(
	        					'%UUID%',
	        					value);
	        			//console.log("Reply is "
	        			//		+ replystr);

	        			var messageBuf = new Buffer(
	        					replystr, 'utf-8');
	        			//console.log(messageBuf.length);
	        			
	        			var sizeBuf = new Buffer(4);
	        			sizeBuf.writeUInt32BE(messageBuf.length, 0);

	        			client.write(sizeBuf);
	        			//console.log(sizeBuf);
	        			client.write(messageBuf);
	        			//console.log(messageBuf);
	        		});
	         
	            });
	        });
	    	
	    }
	});
	
	client.on('close', function() {
		console.log('Connection closed');
	});
	
	

});
