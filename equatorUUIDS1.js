/**
 * Keeps in an array all equator uuids
 */





exports.UUIDs = [
               /* '191B-AF1E-2002-11B2-AAAA10721633' 
                ,'191B-AF1E-2002-11B2-AAAA10721756' 
                ,'191B-AF1E-2002-11B2-AAAA10721879' 
                ,'191B-AF1E-2002-11B2-AAAA10722002' 
                ,'191B-AF1E-2002-11B2-AAAA10722125' 
                ,'191B-AF1E-2002-11B2-AAAA10722248' 
                ,'191B-AF1E-2002-11B2-AAAA10722371' 
                ,'191B-AF1E-2002-11B2-AAAA10722494' 
                ,'191B-AF1E-2002-11B2-AAAA10722617' 
                ,'191B-AF1E-2002-11B2-AAAA10722740' */
                ,'191B-AF1E-2002-11B2-AAAA10722863' 
                ,'191B-AF1E-2002-11B2-AAAA10722986'
                ,'191B-AF1E-2002-11B2-AAAA10723109' 
                ,'191B-AF1E-2002-11B2-AAAA10723232' 
                ,'191B-AF1E-2002-11B2-AAAA10723355' 
                ,'191B-AF1E-2002-11B2-AAAA10723478' 
                ,'191B-AF1E-2002-11B2-AAAA10723601' 
                ,'191B-AF1E-2002-11B2-AAAA10723724' 
                ,'191B-AF1E-2002-11B2-AAAA10723847' 
                ,'191B-AF1E-2002-11B2-AAAA10723970' 
                /*,'191B-AF1E-2002-11B2-AAAA10724093' 
                ,'191B-AF1E-2002-11B2-AAAA10724216' 
                ,'191B-AF1E-2002-11B2-AAAA10724339' 
                ,'191B-AF1E-2002-11B2-AAAA10724462' 
                ,'191B-AF1E-2002-11B2-AAAA10724585' 
                ,'191B-AF1E-2002-11B2-AAAA10724708' 
                ,'191B-AF1E-2002-11B2-AAAA10724831' 
                ,'191B-AF1E-2002-11B2-AAAA10724954' 
                ,'191B-AF1E-2002-11B2-AAAA10725077' 
                ,'191B-AF1E-2002-11B2-AAAA10725200' 
                ,'191B-AF1E-2002-11B2-AAAA10725323' 
                ,'191B-AF1E-2002-11B2-AAAA10725446' 
                ,'191B-AF1E-2002-11B2-AAAA10725569' 
                ,'191B-AF1E-2002-11B2-AAAA10725692' 
                ,'191B-AF1E-2002-11B2-AAAA10725815' 
                ,'191B-AF1E-2002-11B2-AAAA10725938' 
                ,'191B-AF1E-2002-11B2-AAAA10726061' 
                ,'191B-AF1E-2002-11B2-AAAA10726184' 
                ,'191B-AF1E-2002-11B2-AAAA10726307' 
                ,'191B-AF1E-2002-11B2-AAAA10726430' */
                ,'191B-AF1E-2002-11B2-AAAA10726553' 
                ,'191B-AF1E-2002-11B2-AAAA10726676' 
                ,'191B-AF1E-2002-11B2-AAAA10726799' 
                ,'191B-AF1E-2002-11B2-AAAA10726922' 
                ,'191B-AF1E-2002-11B2-AAAA10727045' 
                ,'191B-AF1E-2002-11B2-AAAA10727168' 
                ,'191B-AF1E-2002-11B2-AAAA10727291' 
                ,'191B-AF1E-2002-11B2-AAAA10727414' 
                ,'191B-AF1E-2002-11B2-AAAA10727537'
                ,'191B-AF1E-2002-11B2-AAAA10727660'
                /*,'191B-AF1E-2002-11B2-AAAA10727783' 
                ,'191B-AF1E-2002-11B2-AAAA10727906' 
                ,'191B-AF1E-2002-11B2-AAAA10728029' 
                ,'191B-AF1E-2002-11B2-AAAA10728152' 
                ,'191B-AF1E-2002-11B2-AAAA10728275' 
                ,'191B-AF1E-2002-11B2-AAAA10728398' 
                ,'191B-AF1E-2002-11B2-AAAA10728521' 
                ,'191B-AF1E-2002-11B2-AAAA10728644' 
                ,'191B-AF1E-2002-11B2-AAAA10728767'    
                ,'191B-AF1E-2002-11B2-AAAA10728890'
                ,'191B-AF1E-2002-11B2-AAAA10729013'
                ,'191B-AF1E-2002-11B2-AAAA10729136' 
                ,'191B-AF1E-2002-11B2-AAAA10729259' 
                ,'191B-AF1E-2002-11B2-AAAA10729382' 
                ,'191B-AF1E-2002-11B2-AAAA10729505' 
                ,'191B-AF1E-2002-11B2-AAAA10729628' 
                ,'191B-AF1E-2002-11B2-AAAA10729751' 
                ,'191B-AF1E-2002-11B2-AAAA10729874' 
                ,'191B-AF1E-2002-11B2-AAAA10729997'            
                ,'191B-AF1E-2002-11B2-AAAA10730120'*/ 
                ,'191B-AF1E-2002-11B2-AAAA10730243' 
                ,'191B-AF1E-2002-11B2-AAAA10730366' 
                ,'191B-AF1E-2002-11B2-AAAA10730489' 
                ,'191B-AF1E-2002-11B2-AAAA10730612' 
                ,'191B-AF1E-2002-11B2-AAAA10730735' 
                ,'191B-AF1E-2002-11B2-AAAA10730858' 
                ,'191B-AF1E-2002-11B2-AAAA10730981' 
                ,'191B-AF1E-2002-11B2-AAAA10731104' 
                ,'191B-AF1E-2002-11B2-AAAA10731227' 
                ,'191B-AF1E-2002-11B2-AAAA10731350' 
               /* ,'191B-AF1E-2002-11B2-AAAA10731473' 
                ,'191B-AF1E-2002-11B2-AAAA10731596' 
                ,'191B-AF1E-2002-11B2-AAAA10731719' 
                ,'191B-AF1E-2002-11B2-AAAA10731842' 
                ,'191B-AF1E-2002-11B2-AAAA10731965' 
                ,'191B-AF1E-2002-11B2-AAAA10732088' 
                ,'191B-AF1E-2002-11B2-AAAA10732211' 
                ,'191B-AF1E-2002-11B2-AAAA10732334' 
                ,'191B-AF1E-2002-11B2-AAAA10732457'
                ,'191B-AF1E-2002-11B2-AAAA10732580'*/ 
               /* ,'191B-AF1E-2002-11B2-AAAA10732703' 
                ,'191B-AF1E-2002-11B2-AAAA10732826'
                ,'191B-AF1E-2002-11B2-AAAA10732949' */
                ,'191B-AF1E-2002-11B2-AAAA10733072' 
                ,'191B-AF1E-2002-11B2-AAAA10733195' 
                ,'191B-AF1E-2002-11B2-AAAA10733318'
               /* ,'191B-AF1E-2002-11B2-AAAA10733441' 
                ,'191B-AF1E-2002-11B2-AAAA10733564' 
                ,'191B-AF1E-2002-11B2-AAAA10733687' 
                ,'191B-AF1E-2002-11B2-AAAA10733810' */
              /*  ,'191B-AF1E-2002-11B2-AAAA10733933' 
                ,'191B-AF1E-2002-11B2-AAAA10734056' 
                ,'191B-AF1E-2002-11B2-AAAA10734179' 
                ,'191B-AF1E-2002-11B2-AAAA10734302' 
                ,'191B-AF1E-2002-11B2-AAAA10734425' 
                ,'191B-AF1E-2002-11B2-AAAA10734548' 
                ,'191B-AF1E-2002-11B2-AAAA10734671' 
                ,'191B-AF1E-2002-11B2-AAAA10734794' 
                ,'191B-AF1E-2002-11B2-AAAA10734917' */
              /*  ,'191B-AF1E-2002-11B2-AAAA10735040' 
                ,'191B-AF1E-2002-11B2-AAAA10735163' 
                ,'191B-AF1E-2002-11B2-AAAA10735286' 
                ,'191B-AF1E-2002-11B2-AAAA10735409' 
                ,'191B-AF1E-2002-11B2-AAAA10735532' 
                ,'191B-AF1E-2002-11B2-AAAA10735655' 
                ,'191B-AF1E-2002-11B2-AAAA10735778' 
                ,'191B-AF1E-2002-11B2-AAAA10735901'
                ,'191B-AF1E-2002-11B2-AAAA10736024'
                ,'191B-AF1E-2002-11B2-AAAA10736147'
                ,'191B-AF1E-2002-11B2-AAAA10736270'
                ,'191B-AF1E-2002-11B2-AAAA10736393' 
                ,'191B-AF1E-2002-11B2-AAAA10736516' 
                ,'191B-AF1E-2002-11B2-AAAA10736639' 
                ,'191B-AF1E-2002-11B2-AAAA10736762' 
                ,'191B-AF1E-2002-11B2-AAAA10736885' 
                ,'191B-AF1E-2002-11B2-AAAA10737008' 
                ,'191B-AF1E-2002-11B2-AAAA10737131' 
                ,'191B-AF1E-2002-11B2-AAAA10737254' 
                ,'191B-AF1E-2002-11B2-AAAA10737377' 
               ,'191B-AF1E-2002-11B2-AAAA10737500' 
                ,'191B-AF1E-2002-11B2-AAAA10737623' 
                ,'191B-AF1E-2002-11B2-AAAA10737746' 
                ,'191B-AF1E-2002-11B2-AAAA10737869' 
                ,'191B-AF1E-2002-11B2-AAAA10737992' 
                ,'191B-AF1E-2002-11B2-AAAA10738115' 
                ,'191B-AF1E-2002-11B2-AAAA10738238' 
                ,'191B-AF1E-2002-11B2-AAAA10738361' 
                ,'191B-AF1E-2002-11B2-AAAA10738484' 
                ,'191B-AF1E-2002-11B2-AAAA10738607' 
                ,'191B-AF1E-2002-11B2-AAAA10738730' 
                ,'191B-AF1E-2002-11B2-AAAA10738853' 
                ,'191B-AF1E-2002-11B2-AAAA10738976' 
                ,'191B-AF1E-2002-11B2-AAAA10739099'
                ,'191B-AF1E-2002-11B2-AAAA10739222' 
                ,'191B-AF1E-2002-11B2-AAAA10739345' 
                ,'191B-AF1E-2002-11B2-AAAA10739468' 
                ,'191B-AF1E-2002-11B2-AAAA10739591' 
                ,'191B-AF1E-2002-11B2-AAAA10739714' 
                ,'191B-AF1E-2002-11B2-AAAA10739837'
               /* ,'191B-AF1E-2002-11B2-AAAA10739960' 
                ,'191B-AF1E-2002-11B2-AAAA10740083' 
                ,'191B-AF1E-2002-11B2-AAAA10740206' 
                ,'191B-AF1E-2002-11B2-AAAA10740329' 
                ,'191B-AF1E-2002-11B2-AAAA10740452' 
                ,'191B-AF1E-2002-11B2-AAAA10740575' 
                ,'191B-AF1E-2002-11B2-AAAA10740698' 
                ,'191B-AF1E-2002-11B2-AAAA10740821' 
                ,'191B-AF1E-2002-11B2-AAAA10740944' 
                ,'191B-AF1E-2002-11B2-AAAA10741067' 
                ,'191B-AF1E-2002-11B2-AAAA10741190' 
                ,'191B-AF1E-2002-11B2-AAAA10741313' 
                ,'191B-AF1E-2002-11B2-AAAA10741436' 
                ,'191B-AF1E-2002-11B2-AAAA10741559' 
                ,'191B-AF1E-2002-11B2-AAAA10741682' 
                ,'191B-AF1E-2002-11B2-AAAA10741805' 
                ,'191B-AF1E-2002-11B2-AAAA10741928' 
                ,'191B-AF1E-2002-11B2-AAAA10742051' 
                ,'191B-AF1E-2002-11B2-AAAA10742174' 
                ,'191B-AF1E-2002-11B2-AAAA10742297'
               /* ,'191B-AF1E-2002-11B2-AAAA10742420' 
               ,'191B-AF1E-2002-11B2-AAAA10742543' 
                ,'191B-AF1E-2002-11B2-AAAA10742666' 
                ,'191B-AF1E-2002-11B2-AAAA10742789' 
                ,'191B-AF1E-2002-11B2-AAAA10742912' 
                ,'191B-AF1E-2002-11B2-AAAA10743035' 
                ,'191B-AF1E-2002-11B2-AAAA10743158' 
                ,'191B-AF1E-2002-11B2-AAAA10743281'
                ,'191B-AF1E-2002-11B2-AAAA10743404' 
                ,'191B-AF1E-2002-11B2-AAAA10743527' 
                ,'191B-AF1E-2002-11B2-AAAA10743650' 
                ,'191B-AF1E-2002-11B2-AAAA10743773' 
                ,'191B-AF1E-2002-11B2-AAAA10743896'
                ,'191B-AF1E-2002-11B2-AAAA10744019' 
                ,'191B-AF1E-2002-11B2-AAAA10744142' 
                ,'191B-AF1E-2002-11B2-AAAA10744265' 
                ,'191B-AF1E-2002-11B2-AAAA10744388' 
                ,'191B-AF1E-2002-11B2-AAAA10744511'
                ,'191B-AF1E-2002-11B2-AAAA10744634' 
                ,'191B-AF1E-2002-11B2-AAAA10744757' 
               ,'191B-AF1E-2002-11B2-AAAA10744880' 
                ,'191B-AF1E-2002-11B2-AAAA10745003' 
                ,'191B-AF1E-2002-11B2-AAAA10745126'
                ,'191B-AF1E-2002-11B2-AAAA10745249' 
                ,'191B-AF1E-2002-11B2-AAAA10745372' 
                ,'191B-AF1E-2002-11B2-AAAA10745495' 
                ,'191B-AF1E-2002-11B2-AAAA10745618' 
                ,'191B-AF1E-2002-11B2-AAAA10745741' 
                ,'191B-AF1E-2002-11B2-AAAA10745864' 
                ,'191B-AF1E-2002-11B2-AAAA10745987' 
                ,'191B-AF1E-2002-11B2-AAAA10746110' 
                ,'191B-AF1E-2002-11B2-AAAA10746233' 
                ,'191B-AF1E-2002-11B2-AAAA10746356' 
                ,'191B-AF1E-2002-11B2-AAAA10746479' 
                ,'191B-AF1E-2002-11B2-AAAA10746602' 
                ,'191B-AF1E-2002-11B2-AAAA10746725' 
                ,'191B-AF1E-2002-11B2-AAAA10746848' 
                ,'191B-AF1E-2002-11B2-AAAA10746971' 
                ,'191B-AF1E-2002-11B2-AAAA10747094' 
                ,'191B-AF1E-2002-11B2-AAAA10747217'
               /* ,'191B-AF1E-2002-11B2-AAAA10747340' 
                ,'191B-AF1E-2002-11B2-AAAA10747463' 
                ,'191B-AF1E-2002-11B2-AAAA10747586' 
                ,'191B-AF1E-2002-11B2-AAAA10747709' 
                ,'191B-AF1E-2002-11B2-AAAA10747832' 
                ,'191B-AF1E-2002-11B2-AAAA10747955' 
                ,'191B-AF1E-2002-11B2-AAAA10748078' 
                ,'191B-AF1E-2002-11B2-AAAA10748201' 
                ,'191B-AF1E-2002-11B2-AAAA10748324' 
                ,'191B-AF1E-2002-11B2-AAAA10748447' 
                ,'191B-AF1E-2002-11B2-AAAA10748570' 
                ,'191B-AF1E-2002-11B2-AAAA10748693' 
                ,'191B-AF1E-2002-11B2-AAAA10748816' 
                ,'191B-AF1E-2002-11B2-AAAA10748939' 
                ,'191B-AF1E-2002-11B2-AAAA10749062' 
                ,'191B-AF1E-2002-11B2-AAAA10749185' 
                ,'191B-AF1E-2002-11B2-AAAA10749308' 
                ,'191B-AF1E-2002-11B2-AAAA10749431' 
                ,'191B-AF1E-2002-11B2-AAAA10749554' 
                ,'191B-AF1E-2002-11B2-AAAA10749677' */
               /* ,'191B-AF1E-2002-11B2-AAAA10749800' 
                ,'191B-AF1E-2002-11B2-AAAA10749923' 
                ,'191B-AF1E-2002-11B2-AAAA10750046' 
                ,'191B-AF1E-2002-11B2-AAAA10750169' 
                ,'191B-AF1E-2002-11B2-AAAA10750292' 
                ,'191B-AF1E-2002-11B2-AAAA10750415' 
                ,'191B-AF1E-2002-11B2-AAAA10750538' 
                ,'191B-AF1E-2002-11B2-AAAA10750661' 
                ,'191B-AF1E-2002-11B2-AAAA10750784' 
                ,'191B-AF1E-2002-11B2-AAAA10750907' 
                ,'191B-AF1E-2002-11B2-AAAA10751030' 
                ,'191B-AF1E-2002-11B2-AAAA10751153' 
                ,'191B-AF1E-2002-11B2-AAAA10751276' 
                ,'191B-AF1E-2002-11B2-AAAA10751399' 
                ,'191B-AF1E-2002-11B2-AAAA10751522' 
                ,'191B-AF1E-2002-11B2-AAAA10751645' 
                ,'191B-AF1E-2002-11B2-AAAA10751768' 
                ,'191B-AF1E-2002-11B2-AAAA10751891' 
                ,'191B-AF1E-2002-11B2-AAAA10752014' 
                ,'191B-AF1E-2002-11B2-AAAA10752137' 
                ,'191B-AF1E-2002-11B2-AAAA10752260' 
                ,'191B-AF1E-2002-11B2-AAAA10752383' 
                ,'191B-AF1E-2002-11B2-AAAA10752506' 
                ,'191B-AF1E-2002-11B2-AAAA10752629' 
                ,'191B-AF1E-2002-11B2-AAAA10752752' 
                ,'191B-AF1E-2002-11B2-AAAA10752875' 
                ,'191B-AF1E-2002-11B2-AAAA10752998' 
                ,'191B-AF1E-2002-11B2-AAAA10753121' 
                ,'191B-AF1E-2002-11B2-AAAA10753244' 
                ,'191B-AF1E-2002-11B2-AAAA10753367' 
                ,'191B-AF1E-2002-11B2-AAAA10753490' 
                ,'191B-AF1E-2002-11B2-AAAA10753613' 
                ,'191B-AF1E-2002-11B2-AAAA10753736' 
                ,'191B-AF1E-2002-11B2-AAAA10753859' 
                ,'191B-AF1E-2002-11B2-AAAA10753982' 
                ,'191B-AF1E-2002-11B2-AAAA10754105' 
                ,'191B-AF1E-2002-11B2-AAAA10754228' 
                ,'191B-AF1E-2002-11B2-AAAA10754351' 
                ,'191B-AF1E-2002-11B2-AAAA10754474' 
                ,'191B-AF1E-2002-11B2-AAAA10754597'
              /*  ,'191B-AF1E-2002-11B2-AAAA10754720' 
                ,'191B-AF1E-2002-11B2-AAAA10754843' 
                ,'191B-AF1E-2002-11B2-AAAA10754966' 
                ,'191B-AF1E-2002-11B2-AAAA10755089' 
                ,'191B-AF1E-2002-11B2-AAAA10755212' 
                ,'191B-AF1E-2002-11B2-AAAA10755335' 
                ,'191B-AF1E-2002-11B2-AAAA10755458' 
                ,'191B-AF1E-2002-11B2-AAAA10755581' 
                ,'191B-AF1E-2002-11B2-AAAA10755704' 
                ,'191B-AF1E-2002-11B2-AAAA10755827' 
                ,'191B-AF1E-2002-11B2-AAAA10755950' 
                ,'191B-AF1E-2002-11B2-AAAA10756073' 
                ,'191B-AF1E-2002-11B2-AAAA10756196' 
                ,'191B-AF1E-2002-11B2-AAAA10756319' 
                ,'191B-AF1E-2002-11B2-AAAA10756442' 
                ,'191B-AF1E-2002-11B2-AAAA10756565' 
                ,'191B-AF1E-2002-11B2-AAAA10756688' 
                ,'191B-AF1E-2002-11B2-AAAA10756811' 
                ,'191B-AF1E-2002-11B2-AAAA10756934' 
                ,'191B-AF1E-2002-11B2-AAAA10757057' */
             /*   ,'191B-AF1E-2002-11B2-AAAA10757180' 
                ,'191B-AF1E-2002-11B2-AAAA10757303' 
                ,'191B-AF1E-2002-11B2-AAAA10757426' 
                ,'191B-AF1E-2002-11B2-AAAA10757549' 
                ,'191B-AF1E-2002-11B2-AAAA10757672' 
                ,'191B-AF1E-2002-11B2-AAAA10757795' 
                ,'191B-AF1E-2002-11B2-AAAA10757918' 
                ,'191B-AF1E-2002-11B2-AAAA10758041' 
                ,'191B-AF1E-2002-11B2-AAAA10758164' 
                ,'191B-AF1E-2002-11B2-AAAA10758287' 
                ,'191B-AF1E-2002-11B2-AAAA10758410' 
                ,'191B-AF1E-2002-11B2-AAAA10758533' 
                ,'191B-AF1E-2002-11B2-AAAA10758656' 
                ,'191B-AF1E-2002-11B2-AAAA10758779' 
                ,'191B-AF1E-2002-11B2-AAAA10758902' 
                ,'191B-AF1E-2002-11B2-AAAA10759025' 
                ,'191B-AF1E-2002-11B2-AAAA10759148' 
                ,'191B-AF1E-2002-11B2-AAAA10759271' 
                ,'191B-AF1E-2002-11B2-AAAA10759394' 
                ,'191B-AF1E-2002-11B2-AAAA10759517' 
                ,'191B-AF1E-2002-11B2-AAAA10759640' 
                ,'191B-AF1E-2002-11B2-AAAA10759763' 
                ,'191B-AF1E-2002-11B2-AAAA10759886' 
                ,'191B-AF1E-2002-11B2-AAAA10760009' 
                ,'191B-AF1E-2002-11B2-AAAA10760132' 
                ,'191B-AF1E-2002-11B2-AAAA10760255' 
                ,'191B-AF1E-2002-11B2-AAAA10760378' 
                ,'191B-AF1E-2002-11B2-AAAA10760501' 
                ,'191B-AF1E-2002-11B2-AAAA10760624' 
                ,'191B-AF1E-2002-11B2-AAAA10760747' 
                ,'191B-AF1E-2002-11B2-AAAA10760870' 
                ,'191B-AF1E-2002-11B2-AAAA10760993' 
                ,'191B-AF1E-2002-11B2-AAAA10761116' 
                ,'191B-AF1E-2002-11B2-AAAA10761239' 
                ,'191B-AF1E-2002-11B2-AAAA10761362' 
                ,'191B-AF1E-2002-11B2-AAAA10761485' 
                ,'191B-AF1E-2002-11B2-AAAA10761608' 
                ,'191B-AF1E-2002-11B2-AAAA10761731' 
                ,'191B-AF1E-2002-11B2-AAAA10761854' 
                ,'191B-AF1E-2002-11B2-AAAA10761977'
              /*  ,'191B-AF1E-2002-11B2-AAAA10762100' 
                ,'191B-AF1E-2002-11B2-AAAA10762223' 
                ,'191B-AF1E-2002-11B2-AAAA10762346' 
                ,'191B-AF1E-2002-11B2-AAAA10762469' 
                ,'191B-AF1E-2002-11B2-AAAA10762592' 
                ,'191B-AF1E-2002-11B2-AAAA10762715' 
                ,'191B-AF1E-2002-11B2-AAAA10762838' 
                ,'191B-AF1E-2002-11B2-AAAA10762961' 
                ,'191B-AF1E-2002-11B2-AAAA10763084' 
                ,'191B-AF1E-2002-11B2-AAAA10763207' 
                ,'191B-AF1E-2002-11B2-AAAA10763330' 
                ,'191B-AF1E-2002-11B2-AAAA10763453' 
                ,'191B-AF1E-2002-11B2-AAAA10763576'
                ,'191B-AF1E-2002-11B2-AAAA10763699' 
                ,'191B-AF1E-2002-11B2-AAAA10763822' 
                ,'191B-AF1E-2002-11B2-AAAA10763945' 
                ,'191B-AF1E-2002-11B2-AAAA10764068' 
                ,'191B-AF1E-2002-11B2-AAAA10764191' 
                ,'191B-AF1E-2002-11B2-AAAA10764314' 
                ,'191B-AF1E-2002-11B2-AAAA10764437' 
               /* ,'191B-AF1E-2002-11B2-AAAA10764560' 
                ,'191B-AF1E-2002-11B2-AAAA10764683' 
                ,'191B-AF1E-2002-11B2-AAAA10764806' 
                ,'191B-AF1E-2002-11B2-AAAA10764929' 
                ,'191B-AF1E-2002-11B2-AAAA10765052' 
                ,'191B-AF1E-2002-11B2-AAAA10765175' 
                ,'191B-AF1E-2002-11B2-AAAA10765298' 
                ,'191B-AF1E-2002-11B2-AAAA10765421' 
                ,'191B-AF1E-2002-11B2-AAAA10765544' 
                ,'191B-AF1E-2002-11B2-AAAA10765667' 
                ,'191B-AF1E-2002-11B2-AAAA10765790' 
                ,'191B-AF1E-2002-11B2-AAAA10765913' 
                ,'191B-AF1E-2002-11B2-AAAA10766036'
                ,'191B-AF1E-2002-11B2-AAAA10766159' 
                ,'191B-AF1E-2002-11B2-AAAA10766282' 
                ,'191B-AF1E-2002-11B2-AAAA10766405' 
                ,'191B-AF1E-2002-11B2-AAAA10766528' 
                ,'191B-AF1E-2002-11B2-AAAA10766651' 
                ,'191B-AF1E-2002-11B2-AAAA10766774' 
                ,'191B-AF1E-2002-11B2-AAAA10766897' 
                ,'191B-AF1E-2002-11B2-AAAA10767020' 
                ,'191B-AF1E-2002-11B2-AAAA10767143' 
                ,'191B-AF1E-2002-11B2-AAAA10767266' 
                ,'191B-AF1E-2002-11B2-AAAA10767389' 
                ,'191B-AF1E-2002-11B2-AAAA10767512' 
                ,'191B-AF1E-2002-11B2-AAAA10767635' 
                ,'191B-AF1E-2002-11B2-AAAA10767758' 
                ,'191B-AF1E-2002-11B2-AAAA10767881'
                ,'191B-AF1E-2002-11B2-AAAA10768004' 
                ,'191B-AF1E-2002-11B2-AAAA10768127' 
                ,'191B-AF1E-2002-11B2-AAAA10768250' 
                ,'191B-AF1E-2002-11B2-AAAA10768373' 
                ,'191B-AF1E-2002-11B2-AAAA10768496' 
                ,'191B-AF1E-2002-11B2-AAAA10768619' 
                ,'191B-AF1E-2002-11B2-AAAA10768742' 
                ,'191B-AF1E-2002-11B2-AAAA10768865' 
                ,'191B-AF1E-2002-11B2-AAAA10768988' 
                ,'191B-AF1E-2002-11B2-AAAA10769111' 
                ,'191B-AF1E-2002-11B2-AAAA10769234' 
                ,'191B-AF1E-2002-11B2-AAAA10769357'
                /*,'191B-AF1E-2002-11B2-AAAA10769480' 
                ,'191B-AF1E-2002-11B2-AAAA10769603' 
                ,'191B-AF1E-2002-11B2-AAAA10769726' 
                ,'191B-AF1E-2002-11B2-AAAA10769849' 
                ,'191B-AF1E-2002-11B2-AAAA10769972' 
                ,'191B-AF1E-2002-11B2-AAAA10770095'
                ,'191B-AF1E-2002-11B2-AAAA10770218' 
                ,'191B-AF1E-2002-11B2-AAAA10770341' 
                ,'191B-AF1E-2002-11B2-AAAA10770464' 
                ,'191B-AF1E-2002-11B2-AAAA10770587' 
                ,'191B-AF1E-2002-11B2-AAAA10770710'
                ,'191B-AF1E-2002-11B2-AAAAA9377075' 
                ,'191B-AF1E-2002-11B2-AAAAA9377198' 
                ,'191B-AF1E-2002-11B2-AAAAA9377321' 
                ,'191B-AF1E-2002-11B2-AAAAA9377444' 
                ,'191B-AF1E-2002-11B2-AAAAA9377567' 
                ,'191B-AF1E-2002-11B2-AAAAA9377690' 
                ,'191B-AF1E-2002-11B2-AAAAA9377813'
                ,'191B-AF1E-2002-11B2-AAAAA9377936' 
                ,'191B-AF1E-2002-11B2-AAAAA9378059' */
               /* ,'191B-AF1E-2002-11B2-AAAAA9378182' 
                ,'191B-AF1E-2002-11B2-AAAAA9378305' 
                ,'191B-AF1E-2002-11B2-AAAAA9378428' 
                ,'191B-AF1E-2002-11B2-AAAAA9378551' 
                ,'191B-AF1E-2002-11B2-AAAAA9378674' 
                ,'191B-AF1E-2002-11B2-AAAAA9378797' 
                ,'191B-AF1E-2002-11B2-AAAAA9378920' 
                ,'191B-AF1E-2002-11B2-AAAAA9379043' 
                ,'191B-AF1E-2002-11B2-AAAAA9379166' 
                ,'191B-AF1E-2002-11B2-AAAAA9379289' 
                ,'191B-AF1E-2002-11B2-AAAAA9379412' 
                ,'191B-AF1E-2002-11B2-AAAAA9379535' 
                ,'191B-AF1E-2002-11B2-AAAAA9379658' 
                ,'191B-AF1E-2002-11B2-AAAAA9379781' 
                ,'191B-AF1E-2002-11B2-AAAAA9379904' 
                ,'191B-AF1E-2002-11B2-AAAAA9380027' 
                ,'191B-AF1E-2002-11B2-AAAAA9380150' 
                ,'191B-AF1E-2002-11B2-AAAAA9380273' 
                ,'191B-AF1E-2002-11B2-AAAAA9380396' 
                ,'191B-AF1E-2002-11B2-AAAAA9380519' 
                ,'191B-AF1E-2002-11B2-AAAAA9380642' 
                ,'191B-AF1E-2002-11B2-AAAAA9380765' 
                ,'191B-AF1E-2002-11B2-AAAAA9380888' 
                ,'191B-AF1E-2002-11B2-AAAAA9381011' 
                ,'191B-AF1E-2002-11B2-AAAAA9381134' 
                ,'191B-AF1E-2002-11B2-AAAAA9381257' 
                ,'191B-AF1E-2002-11B2-AAAAA9381380' 
                ,'191B-AF1E-2002-11B2-AAAAA9381503' 
                ,'191B-AF1E-2002-11B2-AAAAA9381626' 
                ,'191B-AF1E-2002-11B2-AAAAA9381749' 
                ,'191B-AF1E-2002-11B2-AAAAA9381872' 
                ,'191B-AF1E-2002-11B2-AAAAA9381995' 
                ,'191B-AF1E-2002-11B2-AAAAA9382118' 
                ,'191B-AF1E-2002-11B2-AAAAA9382241' 
                ,'191B-AF1E-2002-11B2-AAAAA9382364'
                ,'191B-AF1E-2002-11B2-AAAAA9382487' 
                ,'191B-AF1E-2002-11B2-AAAAA9382610' 
                ,'191B-AF1E-2002-11B2-AAAAA9382733' 
                ,'191B-AF1E-2002-11B2-AAAAA9382856' 
                ,'191B-AF1E-2002-11B2-AAAAA9382979'
               /* ,'191B-AF1E-2002-11B2-AAAAA9383102' 
                ,'191B-AF1E-2002-11B2-AAAAA9383225' 
                ,'191B-AF1E-2002-11B2-AAAAA9383348' 
                ,'191B-AF1E-2002-11B2-AAAAA9383471' 
                ,'191B-AF1E-2002-11B2-AAAAA9383594' 
                ,'191B-AF1E-2002-11B2-AAAAA9383717' 
                ,'191B-AF1E-2002-11B2-AAAAA9383840' 
                ,'191B-AF1E-2002-11B2-AAAAA9383963' 
                ,'191B-AF1E-2002-11B2-AAAAA9384086' 
                ,'191B-AF1E-2002-11B2-AAAAA9384209' 
                ,'191B-AF1E-2002-11B2-AAAAA9384332' 
                ,'191B-AF1E-2002-11B2-AAAAA9384455' 
                ,'191B-AF1E-2002-11B2-AAAAA9384578' 
                ,'191B-AF1E-2002-11B2-AAAAA9384701' 
                ,'191B-AF1E-2002-11B2-AAAAA9384824' 
                ,'191B-AF1E-2002-11B2-AAAAA9384947' 
                ,'191B-AF1E-2002-11B2-AAAAA9385070'
                ,'191B-AF1E-2002-11B2-AAAAA9385193' 
                ,'191B-AF1E-2002-11B2-AAAAA9385316'
                ,'191B-AF1E-2002-11B2-AAAAA9385439' */
               /* ,'191B-AF1E-2002-11B2-AAAAA9385562' 
                ,'191B-AF1E-2002-11B2-AAAAA9385685' 
                ,'191B-AF1E-2002-11B2-AAAAA9385808' 
                ,'191B-AF1E-2002-11B2-AAAAA9385931' 
                ,'191B-AF1E-2002-11B2-AAAAA9386054' 
                ,'191B-AF1E-2002-11B2-AAAAA9386177' 
                ,'191B-AF1E-2002-11B2-AAAAA9386300' 
                ,'191B-AF1E-2002-11B2-AAAAA9386423' 
                ,'191B-AF1E-2002-11B2-AAAAA9386546' 
                ,'191B-AF1E-2002-11B2-AAAAA9386669' 
                ,'191B-AF1E-2002-11B2-AAAAA9386792' 
                ,'191B-AF1E-2002-11B2-AAAAA9386915' 
                ,'191B-AF1E-2002-11B2-AAAAA9387038' 
                ,'191B-AF1E-2002-11B2-AAAAA9387161' 
                ,'191B-AF1E-2002-11B2-AAAAA9387284' 
                ,'191B-AF1E-2002-11B2-AAAAA9387407' 
                ,'191B-AF1E-2002-11B2-AAAAA9387530' 
                ,'191B-AF1E-2002-11B2-AAAAA9387653' 
                ,'191B-AF1E-2002-11B2-AAAAA9387776' 
                ,'191B-AF1E-2002-11B2-AAAAA9387899' 
                ,'191B-AF1E-2002-11B2-AAAAA9388022' 
                ,'191B-AF1E-2002-11B2-AAAAA9388145' 
                ,'191B-AF1E-2002-11B2-AAAAA9388268' 
                ,'191B-AF1E-2002-11B2-AAAAA9388391' 
                ,'191B-AF1E-2002-11B2-AAAAA9388514' 
                ,'191B-AF1E-2002-11B2-AAAAA9388637' 
                ,'191B-AF1E-2002-11B2-AAAAA9388760' 
                ,'191B-AF1E-2002-11B2-AAAAA9388883' 
                ,'191B-AF1E-2002-11B2-AAAAA9389006' 
                ,'191B-AF1E-2002-11B2-AAAAA9389129'*/
                ];



//'191B-AF1E-2002-11B2-7CC3A1719D3B',
