var net = require('net');
var fs = require('fs');
var smalloc = require('smalloc');
var UUIDSFile = require('./equatorUUIDS.js');

var SIZE_LENGTH = 4; // sizeof(uint32)
var HOST = 'fusionaws.clarecontrols.com';
var PORT = 8888;

function writeResponse(uuid, client, reply, params) {
	var replyStr = reply.toString();
	if (params) {
		for (var key in params) {
			if (params.hasOwnProperty(key)) {
				replyStr = replyStr.replace(key, params[key]);
			}
		}
	}
	var messageBuf = new Buffer(replyStr, 'utf8');
	var sizeBuf = new Buffer(SIZE_LENGTH);
	sizeBuf.writeUInt32BE(messageBuf.length, 0);
	client.write(sizeBuf);
	client.write(messageBuf);
	//console.log('>>>>> Wrote response from ' + uuid + ': size=' + messageBuf.length + ' - ' + replyStr);
	console.log(new Date().toString());
	console.log('>>>>> Wrote response from ' + uuid + ': size=' + messageBuf.length);

}

function writeFileResponse(uuid, client, file, params) {
	fs.readFile(file, function(err, reply) {
		writeResponse(uuid, client, reply, params);
	});
}

function readSize(buf) {
	// First SIZE_LENGTH bytes are the message size
	var sizeBuf = new Buffer(SIZE_LENGTH);
	buf.copy(sizeBuf, 0, 0, SIZE_LENGTH);
	return sizeBuf.readUInt32BE(0);
}

function consumeMessage(inBuf, msgBuf, msgSize) {
	inBuf.copy(msgBuf, 0, SIZE_LENGTH, msgSize + SIZE_LENGTH);
	var newInBuf = new Buffer(inBuf.length);
	inBuf.copy(newInBuf, 0, msgSize + SIZE_LENGTH);
	return newInBuf;
}

function handleMessage(uuid, client, jsonObj) {
	if (jsonObj.result != null && jsonObj.result == "OK") {
		console.log('Connected! ' + uuid);
	} else if (jsonObj.method != null) {
		var responseFile = null;
		var responseParams = {'%UUID%': uuid, '%REQUESTID%': jsonObj.id};
		if (jsonObj.method === 'HDAdmin/getProtocolAdapterInfos') {
			responseFile = './mock_data/HDAdmin_getProtocolAdapterInfos_UUID.json';
		} else if (jsonObj.method === 'HAM/getCommandTypes') {
			responseFile = './mock_data/HAM_getCommandTypes_UUID.json';
		} else if (jsonObj.method === 'HAM/getConditionTypes') {
			responseFile = './mock_data/HAM_getCommandTypes_UUID.json';
		} else if (jsonObj.method === 'configuration.import') {
			responseFile = './mock_data/configurationImportReply_UUID.json';
			fs.writeFile('./docs/' + uuid + '.zip', new Buffer(jsonObj.params[0], 'base64'));
		}
		if (responseFile) {
			writeFileResponse(uuid, client, responseFile, responseParams);
		}
	}
}

UUIDSFile.UUIDs.forEach(function(uuid) {
	var client = new net.Socket();
	var incomingBuf = new Buffer(smalloc.kMaxLength);
	var writeOffset = 0;
	client.connect(PORT, HOST, function() {
		console.log('Connecting to ' + HOST + ':' + PORT +'... ' + uuid);
		writeFileResponse(uuid, client, './mock_data/handshake_UUID.json', {'%UUID%': uuid});
	});
	client.on('data', function(data) {
		// Copy new data into incoming buffer
		data.copy(incomingBuf, writeOffset);
		writeOffset += data.length;
		// Read messages from the incoming buffer
		while (writeOffset >= SIZE_LENGTH) {
			var msgSize = readSize(incomingBuf);
			if (writeOffset >= msgSize + SIZE_LENGTH) {
				// We have a complete message, consume it
				var messageBuf = new Buffer(msgSize);
				incomingBuf = consumeMessage(incomingBuf, messageBuf, msgSize);
				writeOffset -= (msgSize + SIZE_LENGTH);
				// Parse the message as a JSON object
				console.log(new Date().toString());
				console.log('<<<<< Received message for ' + uuid + ': size=' + msgSize + ' - ' + messageBuf.toString());
				handleMessage(uuid, client, JSON.parse(messageBuf.toString()));
			} else {
				// Incomplete message, wait until we get more data
				break;
			}
		}
	});
	client.on('error', function(e) {
		if (e.code === 'ECONNREFUSED' || e.code === 'ECONNRESET') {
			if (e.code === 'ECONNREFUSED') {
				console.log('Is a server running at ' + HOST + ':' + PORT + '?');
			}
			var delaySecs = 4;
			client.setTimeout(delaySecs * 1000, function() {
				client.connect(PORT, HOST, function() {
					console.log('Re-connecting to ' + HOST + ':' + PORT +'... ' + uuid);
					writeFileResponse(uuid, client, './mock_data/handshake_UUID.json', {'%UUID%': uuid});
				});
			});
			console.log('Waiting for ' + delaySecs + ' seconds before retrying ' + HOST + ':' + PORT + ' again... ' + uuid);
		}
	});
	client.on('close', function() {
		console.log('Connection closed! ' + uuid);
	});
});
