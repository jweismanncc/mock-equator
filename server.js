var http = require("http"),
    RPCHandler = require("jsonrpc").RPCHandler
    fs = require("fs");

var qs = require('querystring');

// start server
http.createServer(function (request, response) {
    //console.log(request.headers['content-type']);
    if (request!=undefined && request.headers != undefined && request.headers['content-type'] != undefined){
        var contenttype = request.headers['content-type'];
        if ((contenttype.indexOf("json") != -1)  && request.method == "POST"){
        	
            new RPCHandler(request, response, RPCMethods, true);
        }
        else if (contenttype.indexOf("multipart") != -1){
            response.writeHead("200");
            fs.readFile("./mock_data/CCSendEvents_postEvent.json", function(err, data){
            	 response.write(data);
            	 response.end();
              });
           
        }
    }
    else{
        response.end('I hear the weather in Greece is nice this time of year');
    }
}).listen(8080);

RPCMethods = {
  "system.listMethods": function(rpc){
    fs.readFile("./mock_data/system_listMethods.json", function(err, data){
      data = JSON.parse(data);
      rpc.response(data.result);
    });
  },
  "CCConfigureZWave/stopZwaveDiags": function(rpc){
      rpc.response(true);
  },
  "HDAdmin/getProtocolAdapterInfos": function(rpc){
    fs.readFile("./mock_data/HDAdmin_getProtocolAdapterInfos.json", function(err, data){
      data = JSON.parse(data);
      rpc.response(data.result);
    });
  },
  "CCSendEvents/postEvent": function(rpc){
	  
	  //fs.readFile("./mock_data/CCSendEvents_postEvent.json", function(err, data){
	   rpc.response("");
	     
	    //});
  },
  "HAM/getCommandTypes": function(rpc){
    fs.readFile("./mock_data/HAM_getCommandTypes.json", function(err, data){
      data = JSON.parse(data);
      rpc.response(data.result);
    });
  },
  "HAM/getConditionTypes": function(rpc){
    fs.readFile("./mock_data/HAM_getConditionTypes.json", function(err, data){
      data = JSON.parse(data);
      rpc.response(data.result);
     });
  },

  _private: function(){}
}